import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-dialog-delete',
  templateUrl: './dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.scss']
})
export class DialogDeleteComponent implements OnInit {

	constructor(public dialogRef : MatDialogRef<DialogDeleteComponent>, @Inject(MAT_DIALOG_DATA) public data: any){
	} 

	ngOnInit() {
	}

	// yes method is used to close the delete dialog and send the response "yes".
	yes(){
		this.dialogRef.close("yes");
	}
}