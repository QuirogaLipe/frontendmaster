export class Document {
  constructor(
    public id?: number,
    public name?: string,
    public path?: string,
    public id_user?: string,
    public status?: string
  ){}
}