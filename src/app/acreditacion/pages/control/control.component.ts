import { AuthService } from 'app/service/auth-service/auth.service';
import { MasterService } from 'app/acreditacion/services/master.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { CoreService } from 'app/service/core/core.service';
import { dataLoader } from '@amcharts/amcharts4/core';

@Component({
  selector: 'ms-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  process: any[] = []
  fases: any[] = []
  tasks: any[] = []

  id_process:number

  user: any

  processData: any = {
    created: '',
    description: '',
    documents_list: '',
    id: '',
    last_updated: '',
    name: '',
    name_user: "",
    status: "",
    user_list: "",
    status_process:'0'
  }

  projectStatus: any[] = []

  //sale chart data 
  saleChartData: any []= []




  constructor(private pageTitleService: PageTitleService,
    private masterData: MasterService,
    private auth: AuthService,
    private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.user = this.auth.getLSuser()
    this.pageTitleService.setTitle("Tablero de control");

    if(this.user.user_type !== undefined){
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      }
      else {
        this.loadProcesss()
      }
    }
  }
  loadProcesssGeneral() {
    console.log("userrrrr", this.user)
    this.masterData.getProcesss().subscribe((data: any) => {
      console.log(data)
      if(data===undefined){
        return 
      }
      this.process = data
      this.id_process = this.process[0].id
      this.loadPhases(this.id_process)
    },
      error => {
        console.log(error)
      })
  }
  loadProcesss() {
    console.log("userrrrr", this.user)
    this.masterData.getProcesssByUser(this.user.id).subscribe((data: any) => {
      console.log(data)
      if(data===undefined){
        return 
      }
      this.process = data
      this.id_process = this.process[0].id
      this.loadPhases(this.id_process)
    },
      error => {
        console.log(error)
      })
  }
  loadPhases(idprocess) {
    console.log("id",this.id_process)

    let pro = this.process.find( element => {
      return element.id == this.id_process ? element: null;
    });
    console.log("procesoooo:", pro)

    this.processData= pro

    this.masterData.getPhasesByProcess(this.id_process).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      if(!data)
        return;
      this.fases = data
      console.log("fasesss",this.fases)
      this.saleChartData = []
      this.fases.forEach(element => {
        this.saleChartData.push({
          id: element.id,
          title: element.name,
          value: parseInt(element.status_phase) 
        })
      });
      this.loadTask(data[0].id)
      this.cd.detectChanges()
      console.log("isaleds",this.saleChartData)

    },
      error => {
        console.log(error)
      })
  }
  loadTask(idfase) {


    this.masterData.getTaskByPhase(idfase).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.tasks = data
      this.projectStatus = []
      this.tasks.forEach(element => {
        let color = element.status_task == '0'? 'warn-bg' : (element.status_task == '1' ? 'primary-bg': 'success-bg')
        this.projectStatus.push({
          id: element.id,
          title: element.name,
          color: color,
          duration: element.status_task == '0'? 'Sin iniciar' : (element.status_task == '1' ? 'En proceso': 'Completado'),
          value: element.status_task == '0'? 0 : (element.status_task == '1' ? 50: 100)
        })
      });
      this.cd.detectChanges()
      console.log("isaleds",this.projectStatus)


    },
      error => {
        console.log(error)
      })
  }
  parse(val:string){
    return parseFloat(val)
  }
}