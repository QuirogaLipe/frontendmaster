import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { FormIndicatorComponent } from './form-indicator/form-indicator.component';

@Component({
  selector: 'ms-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss']
})
export class IndicatorComponent implements OnInit {
  @Input() processPadre : string

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['id', 'name', 'description','create','status', 'action'];
  dataSource: any

  id_process = ''


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar : ActivatedRoute
  ) {
    //this.id_process = this.ar.snapshot.paramMap.get('id')

  }

  ngOnInit() {
    this.loadIndicators()
  }
  openIndicator(data){
    this.route.navigate(['indicator/'+data.id])
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadIndicators() {
    this.id_process = this.masterService.idprocess
    this.isLoad = true
    this.masterService.getIndicatorsByProcess(this.id_process).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      
      let indicators:any[] =[] 
      data.forEach(element => {
        if(element.length != 0){
          element.forEach(element => {
            indicators.push(element)
          });
        }
      });
      console.log("indicadores",indicators)
      this.dataSource = new MatTableDataSource(indicators)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetIndicator(data: any) {
    console.log(data)
    this.isLoad = true
    this.masterService.addIndicator(data).subscribe(() => {
      this.loadIndicators()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditIndicator(data: any) {
    this.isLoad = true

    this.masterService.updateIndicator(data).subscribe(() => {
      this.loadIndicators()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Indicator1: Indicator = new Indicator(data.id,data.name, status)
    this.onEditIndicator(Indicator1) */
  }
  onDeleteIndicator(id: any) {
    this.isLoad = true
    this.masterService.deleteIndicator(id).subscribe(() => {
      this.loadIndicators()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id : data.id,
        id_criteria: data.id_criteria,
        name:data.name,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormIndicatorComponent, {
      data: rpv1,
      width: '500px'
    }) 

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetIndicator(result)
        } else {
          this.onEditIndicator(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar  de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteIndicator(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}