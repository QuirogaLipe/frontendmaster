import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';

@Component({
  selector: 'ms-form-indicator',
  templateUrl: './form-indicator.component.html',
  styleUrls: ['./form-indicator.component.scss']
})
export class FormIndicatorComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  list_criteria : any [] = []

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      id : null,
        id_criteria: null,
        name:null,
        description: null,
        status: '1'
    })
  }

  ngOnInit() {
    this.initForm()
    
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      console.log("actualizare", this.data)
      this.id = this.data.id
      this.form.setValue(this.data)
    }
    this.loadCriterias(this.masterService.idprocess)
  }

  onSubmit(data: any) {
    console.log(data)
    if (this.form.valid) {


      console.log(this.form.value)
      this.dialogRef.close(this.form.value)
      this.form.reset()
    }
  }
  private loadCriterias(id) {
    this.masterService.getCriteriasByProcess(id).subscribe((data: any) => {
      console.log(data)
      data.forEach(element => {
        this.list_criteria.push({
          id: element.id.toString(),
          name: element.name
        })
      });
    },
      error => {
       console.log(error)
      })
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

}
