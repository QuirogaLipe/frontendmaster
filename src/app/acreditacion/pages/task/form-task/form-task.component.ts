import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormPhaseComponent } from '../../process/form-phase/form-phase.component';
import { MasterService } from 'app/acreditacion/services/master.service';
@Component({
  selector: 'ms-form-task',
  templateUrl: './form-task.component.html',
  styleUrls: ['./form-task.component.scss']
})
export class FormTaskComponent implements OnInit {

  public form: FormGroup
  public id: any = ''
  public users: any[] = []
  indicators:any[]=[]
  criterias:any []= []
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      id: null,
      id_indicator: null,
      name: null,
      start_date: null,
      end_date: null,
      id_criteria: null,
      //close_date: null,
      status_task: '0',
      user_list: null,
      description: null,
      status: '1',
    })
  }

  ngOnInit() {
    this.initForm()
    this.getUsers()
    this.loadCriterias()
    //this.getIndicators()
  }
  getUsers() {
    this.masterService.getUsersByProcess(this.masterService.idprocess).subscribe((data: any) => {
      this.users = data
    })
  }
  getIndicators(data) {
    this.masterService.getIndicatorsByCriteria(data).subscribe((data: any) => {
      console.log(data)
      this.indicators = data
    })
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      console.log("actualizare", this.data)
      this.id = this.data.id
      this.data.user_list = this.data.user_list.split(',')
      this.form.setValue(this.data)
      this.form.get('id_criteria').setValue(parseInt(this.data.id_criteria))
      this.form.get('id_indicator').setValue(parseInt(this.data.id_indicator))
      this.getIndicators(this.data.id_indicator)
    }
  }

  selectIndicatorByCriterio(data){
    this.getIndicators(data)
  }

  onSubmit(data: any) {
    console.log(data)
    if (this.form.valid) {

      console.log(this.form.value)

      this.dialogRef.close({
        id: this.form.value.id,
        id_indicator: this.form.value.id_indicator,
        name: this.form.value.name,
        start_date: this.form.value.start_date,
        end_date: this.form.value.end_date,
        //close_date: this.form.value.close_date,
        status_task: this.form.value.status_task,
        user_list: this.form.value.user_list.toString(),
        description: this.form.value.description,
        status: this.form.value.status
      });
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

  loadCriterias() {
    this.masterService.getCriteriasByProcess(this.masterService.idprocess).subscribe((data: any) => {
      console.log(data)
      this.criterias = data

    },
      error => {
        console.log(error)
      })
  }



}

