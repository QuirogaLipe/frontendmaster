import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';
import { FormTaskComponent } from './form-task/form-task.component';
import { DialogComponent } from 'app/acreditacion/dialog/dialog.component';


@Component({
  selector: 'ms-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false

  displayedColumns: string[] = ['id', 'name', 'description','name_criteria','name_indicator', 'start_date', 'end_date', 'status', 'action'];
  dataSource: any

  //id_process = ''
  idphase= ''

  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar: ActivatedRoute
  ) {
    //this.id_process = this.ar.snapshot.paramMap.get('id')
  }

  ngOnInit() {
    this.idphase=this.masterService.idphases
    this.loadTasks()
  }
  openTask(data) {
    this.route.navigate(['task/' + data.id])
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadTasks() {

    this.isLoad = true
/*     this.masterService.getTasks().subscribe((data: any) => { */
      this.masterService.getTaskByPhase(this.idphase).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log("tareasss", data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetTask(data: any) {
    console.log(data)
    this.isLoad = true
    this.masterService.addTask({
      id: data.id,
        id_indicator: data.id_indicator,
        name: data.name,
        start_date: data.start_date,
        end_date: data.end_date,
        //close_date: data.close_date,
        id_phase: this.idphase,
        status_task: data.status_task,
        user_list: data.user_list,
        description: data.description,
        status: data.status
    }
    ).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      console.log(error)
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditTask(data: any) {
    this.isLoad = true

    this.masterService.updateTask(data).subscribe(() => {
      this.loadTasks()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: any,id) {
    this.masterService.setStatusTask(data,id).subscribe(() => {
      this.loadTasks()
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
    
  }
  onDeleteTask(id: any) {
    this.isLoad = true
    this.masterService.deleteTask(id).subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id: data.id,
        id_indicator: data.id_indicator,
        name: data.name,
        start_date: data.start_date,
        end_date: data.end_date,
        //close_date: data.close_date,
        id_criteria : data.id_criteria,
        status_task: data.status_task,
        user_list: data.user_list,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormTaskComponent, {
      data: rpv1,
      width: '700px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetTask(result)
        } else {
          this.onEditTask(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent, { data: "¿Estás seguro de que quieres eliminar  de forma permanente?", width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteTask(i.id)
      }
    })
  }

  onSetStatus(element,i: any) {
    let str = i == '1'? 'En Proceso' : 'Realizada'
    const dialogRef = this.dialog.open(DialogComponent, { data:{
      title: "Cambiar estado de una tarea",
      content: `¿Estas seguro que deseas marcar esta tarea como '${str}'? `
    }, width: '450px' })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        let obj:any = new Object(element)
        //this.onEditTask(obj)
        this.onUpdateStatus({status_task:i},element.id)
      }
    })
  }

  closeFase() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: "¿Seguro que desea cerrar la fase?",
        content: "Se enviara un correo a los usuarios"
      }, width: '450px'
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onsetNotification()
      }
    })
  }
  onsetNotification() {
    this.isLoad = true
    this.masterService.sendNotification().subscribe(() => {
      this.loadTasks()
      this.isLoad = false
      this.openSnackBar("Enviado", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

  changeDate(element){
    return (new Date(element)).getFullYear()+'-'+(new Date(element)).getMonth()+'-'+(new Date(element)).getDay()
  }
}