import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormPhaseComponent } from './form-phase/form-phase.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';


@Component({
  selector: 'ms-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss']
})
export class ProcessComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['id', 'name', 'description','status_phase','create','finished','status', 'action'];
  dataSource: any

  id_process = ''


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar : ActivatedRoute,public pageTitleService: PageTitleService
  ) {
    this.id_process = this.ar.snapshot.paramMap.get('id')
    console.log(this.id_process)
    this.masterService.idprocess = this.id_process
  }

  ngOnInit() {
    this.pageTitleService.setTitle("Proceso " + this.id_process)
    this.loadPhases(this.id_process)
  }
  openPhase(data){
    this.route.navigate(['phase/'+data.id])
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadPhases(id) {

    this.isLoad = true
    this.masterService.getPhasesByProcess(id).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetPhase(data: any) {
    console.log(data)
    this.isLoad = true
    this.masterService.addPhase({
      id : data.id,
      name:data.name,
      id_process : this.id_process,
      finished: data.finished,
      description: data.description,
      status: data.status
    }).subscribe(() => {
      this.loadPhases(this.id_process)
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditPhase(data: any) {
    this.isLoad = true

    this.masterService.updatePhase(data).subscribe(() => {
      this.loadPhases(this.id_process)
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Phase1: Phase = new Phase(data.id,data.name, status)
    this.onEditPhase(Phase1) */
  }
  onDeletePhase(id: any) {
    this.isLoad = true
    this.masterService.deletePhase(id).subscribe(() => {
      this.loadPhases(this.id_process)
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id : data.id,
        name:data.name,
        finished: data.finished,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormPhaseComponent, {
      data: rpv1,
      width: '500px'
    }) 

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetPhase(result)
        } else {
          this.onEditPhase(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar  de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeletePhase(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}