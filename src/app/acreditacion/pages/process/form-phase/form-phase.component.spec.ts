import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPhaseComponent } from './form-phase.component';

describe('FormPhaseComponent', () => {
  let component: FormPhaseComponent;
  let fixture: ComponentFixture<FormPhaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPhaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPhaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
