import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'ms-form-phase',
  templateUrl: './form-phase.component.html',
  styleUrls: ['./form-phase.component.scss']
})
export class FormPhaseComponent implements OnInit {

  public form: FormGroup
  public id: any = ''

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormPhaseComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService) {
    this.form = this.fb.group({
      id: null,
      name: null,
      description: null,
      finished: null,
      status: '1'
    })
  }

  ngOnInit() {
    this.initForm()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id = null

    }
    else {
      console.log("actualizare", this.data)
      this.id = this.data.id
      this.form.setValue({
        id: this.data.id,
        name: this.data.name,
        description: this.data.description,
        finished: this.data.finished,
        status: this.data.status
      })
    }
  }

  onSubmit(data: any) {
    console.log(data)
    if (this.form.valid) {


      console.log(this.form.value)
      this.dialogRef.close(this.form.value)
      this.form.reset()
    }
  }
  onClose() {
    this.dialogRef.close('Close')
    this.form.reset()
  }

}