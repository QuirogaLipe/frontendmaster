import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { MasterService } from 'app/acreditacion/services/master.service';
import { AuthService } from 'app/service/auth-service/auth.service';

@Component({
  selector: 'ms-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  process : any = []
  user:any
  id_process:any = ''

  constructor(private pageTitleService: PageTitleService,
    private masterData: MasterService,
    private auth: AuthService) { }

  ngOnInit() {
    this.user = this.auth.getLSuser()
    this.pageTitleService.setTitle("Reportes");

    if(this.user.user_type !== undefined){
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      }
      else {
        this.loadProcesss()
      }
    }
  }

  loadProcesssGeneral() {
    console.log("userrrrr", this.user)
    this.masterData.getProcesss().subscribe((data: any) => {
      console.log(data)
      if(data===undefined){
        return 
      }
      this.process = data
    },
      error => {
        console.log(error)
      })
  }
  loadProcesss() {
    console.log("userrrrr", this.user)
    this.masterData.getProcesssByUser(this.user.id).subscribe((data: any) => {
      console.log(data)
      if(data===undefined){
        return 
      }
      this.process = data

    },
      error => {
        console.log(error)
      })
  }

}
