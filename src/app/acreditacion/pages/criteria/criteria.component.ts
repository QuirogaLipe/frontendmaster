import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormCriteriaComponent } from './form-criteria/form-criteria.component';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterService } from 'app/acreditacion/services/master.service';

@Component({
  selector: 'ms-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.scss']
})
export class CriteriaComponent implements OnInit {
  @Input() id_processPadre : string

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false
  displayedColumns: string[] = ['id', 'name', 'description','create','status', 'action'];
  dataSource: any

  id_process = ''


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router, private ar : ActivatedRoute
  ) {
    
    console.log("id:",this.id_processPadre)
    //this.id_process = this.id_processPadre
  }

  ngOnInit() {
    this.id_process = this.masterService.idprocess
    this.loadCriterias(this.id_process)
  }
  openCriteria(data){
    this.route.navigate(['criteria/'+data.id])
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadCriterias(id) {

    this.isLoad = true
    /* this.masterService.getCriterias().subscribe((data: any) => { */
    this.masterService.getCriteriasByProcess(id).subscribe((data: any) => {
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetCriteria(data: any) {
    console.log(data)
    this.isLoad = true
    this.masterService.addCriteria({
      id : data.id,
      name:data.name,
      id_process : this.id_process,
      finished: data.finished,
      description: data.description,
      status: data.status
    }).subscribe((data) => {
      console.log(data)
      this.loadCriterias(this.id_process)
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditCriteria(data: any) {
    this.isLoad = true

    this.masterService.updateCriteria(data).subscribe(() => {
      this.loadCriterias(this.id_process)
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Criteria1: Criteria = new Criteria(data.id,data.name, status)
    this.onEditCriteria(Criteria1) */
  }
  onDeleteCriteria(id: any) {
    this.isLoad = true
    this.masterService.deleteCriteria(id).subscribe(() => {
      this.loadCriterias(this.id_process)
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {

    const dialogRef = this.dialog.open(FormCriteriaComponent, {
      data: data,
      width: '500px'
    }) 

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetCriteria(result)
        } else {
          this.onEditCriteria(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar  de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteCriteria(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}