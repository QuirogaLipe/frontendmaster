import { AuthService } from 'app/service/auth-service/auth.service';
import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-form-process',
  templateUrl: './form-process.component.html',
  styleUrls: ['./form-process.component.scss']
})
export class FormProcessComponent implements OnInit {

  public form: FormGroup
  public id:any =''
  public users: any []= []

  public user: any 

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<FormProcessComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private masterService: MasterService , private auth:AuthService) {
    this.user = this.auth.getLSuser().id
    console.log(this.user)
    this.form = this.fb.group({
      id : null,
        name:null,
        id_user: this.user,
        user_list: null,
        documents_list: '',
        description: null,
        status: '1'
    })
  }

  ngOnInit() {
    this.initForm()
    this.getUsers()
  }

  getUsers(){
    this.masterService.getUsers().subscribe((data:any)=> {
      console.log(data)
      this.users = data
    })
  }
  getId(){
    return this.user.toString()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id=null
      
    }
    else {
      console.log("actualizare",this.data)
      this.id=this.data.id
      this.form.setValue({
        id : this.data.id,
        name:this.data.name,
        id_user: this.data.id_user,
        user_list: this.data.user_list.split(','),
        documents_list: '',
        description: this.data.description,
        status: this.data.status
      })
    }
  }

  onSubmit(data:any) {
    console.log(data)
    if(this.form.valid){
      
     
      console.log(this.form.value)
      this.dialogRef.close({
        id : this.form.value.id,
        name:this.form.value.name,
        id_user: this.form.value.id_user,
        user_list: this.form.value.user_list.toString(),
        documents_list: this.form.value.documents_list,
        description: this.form.value.description,
        status: this.form.value.status
      })
      this.form.reset()
    }
  }
  onClose(){
    this.dialogRef.close('Close')
    this.form.reset()
  }

}