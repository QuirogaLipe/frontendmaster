import { AuthService } from 'app/service/auth-service/auth.service';
import { Router } from '@angular/router';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { MasterService } from 'app/acreditacion/services/master.service';
import { DialogDeleteComponent } from 'app/acreditacion/dialog-delete/dialog-delete.component';
import { FormProcessComponent } from './form-process/form-process.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';

@Component({
  selector: 'ms-evaluaciones',
  templateUrl: './evaluaciones.component.html',
  styleUrls: ['./evaluaciones.component.scss']
})
export class EvaluacionesComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort

  isLoad: boolean = false


  displayedColumns: string[] = ['id', 'name','id_user', 'description','status', 'action'];
  dataSource: any
  user:any


  constructor(private masterService: MasterService,
    private dialog: MatDialog, private snackBar: MatSnackBar,
    private route: Router ,public pageTitleService: PageTitleService,
    private auth: AuthService
  ) { 
    this.pageTitleService.setTitle("Procesos")
  }

  ngOnInit() {
    this.user=this.auth.getLSuser()
    if(this.user.user_type !== undefined){
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      }
      else {
        this.loadProcesss()
      }
    }
  }
  openProcess(data){
    this.route.navigate(['process/'+data.id])
  }
  applyFilter(filterValue: String) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  private loadProcesssGeneral() {

    this.isLoad = true
    this.masterService.getProcesss().subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  private loadProcesss() {

    this.isLoad = true
    this.masterService.getProcesssByUser(this.user.id).subscribe((data: any) => {
      /* this.nts = data
      this.nts.sort(function (obj1, obj2) {
        return obj2.id - obj1.id
      }) */
      console.log(data)
      this.dataSource = new MatTableDataSource(data)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoad = false
    },
      error => {
        this.openSnackBar(error.name, 'Error')
        this.isLoad = false
      })
  }
  onSetProcess(data: any) {
    console.log(data)
    this.isLoad = true
    this.masterService.addProcess(data).subscribe(() => {
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      } else {
        this.loadProcesss()
      }
     
      this.isLoad = false
      this.openSnackBar("Se añadio correctamente", "ÉXITO")
    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onEditProcess(data: any) {
    this.isLoad = true

    this.masterService.updateProcess(data).subscribe(() => {
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      } else {
        this.loadProcesss()
      }
      this.openSnackBar("Se editó corectamente", "ÉXITO")

    }, error => {
      this.isLoad = false
      this.openSnackBar(error.name, 'Error')

    })
  }
  onUpdateStatus(data: any) {

    /* let status: string = (~~(!parseInt(data.status))).toString()
    let Process1: Process = new Process(data.id,data.name, status)
    this.onEditProcess(Process1) */
  }
  onDeleteProcess(id: any) {
    this.isLoad = true
    this.masterService.deleteProcess(id).subscribe(() => {
      if(this.user.user_type == "1"){
        this.loadProcesssGeneral()
      } else {
        this.loadProcesss()
      }
      this.isLoad = false
      this.openSnackBar("Se eliminó correctamente", "ÉXITO")
    }, error => {
      this.openSnackBar(error.name, 'Error')
      this.isLoad = false

    })
  }

  openModal(data: any) {
    let rpv1: any
    if (data) {
      rpv1 = {
        id : data.id,
        name:data.name,
        id_user: data.id_user,
        user_list: data.user_list,
        documents_list: data.documents_list,
        description: data.description,
        status: data.status
      }
    } else {
      rpv1 = null
    }

    const dialogRef = this.dialog.open(FormProcessComponent, {
      data: rpv1,
      width: '500px'
    }) 

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined || result == 'Close') {
        console.log('close')
      } else {
        if (result.id == null) {
          this.onSetProcess(result)
        } else {
          this.onEditProcess(result)
        }
      }
    })
  }
  // pregunta de eliminar
  onDelete(i: any) {
    const dialogRef = this.dialog.open(DialogDeleteComponent,{data:"¿Estás seguro de que quieres eliminar este proceso de forma permanente?", width:'450px'})

    dialogRef.afterClosed().subscribe(result => {
      if (result == "yes") {
        this.onDeleteProcess(i.id)
      }
    })
  } 

  //snackBar
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }
}