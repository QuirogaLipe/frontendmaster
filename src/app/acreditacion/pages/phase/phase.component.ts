import { MasterService } from './../../services/master.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';

@Component({
  selector: 'ms-phase',
  templateUrl: './phase.component.html',
  styleUrls: ['./phase.component.scss']
})
export class PhaseComponent implements OnInit {
  id_phase = ''

  constructor(public ar : ActivatedRoute, private masterData: MasterService, public pageTitleService: PageTitleService) {
    this.id_phase = this.ar.snapshot.paramMap.get('id')
    this.masterData.idphases=this.id_phase
  }
  ngOnInit() {
    this.pageTitleService.setTitle("Fase: "+this.id_phase)
  }
}
