import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  private domain: string ="http://localhost:8000/"
  //private domain: string ="http://192.168.43.30:8000/"
 
  
   constructor() { }
 
   getDomain():string{
 
     return this.domain
   }
 
 }