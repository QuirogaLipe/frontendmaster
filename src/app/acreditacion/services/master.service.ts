import { AuthService } from './../../services/auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomainService } from './domain.service';
import { Document } from './../models/document'
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  //TO DO: aumentar auth services en el constructor
  constructor(private http: HttpClient, private dom: DomainService, private authService: AuthService) { }


  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'TOKEN ' + this.authService.getToken()
  })

  // parte Sin dialog opcion 2:

  getDocuments() {
    const url_api = this.dom.getDomain() + 'api/document/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getDocument(id: string) {
    console.log("ontener id")
    const url_api = this.dom.getDomain() + 'api/document/get/select/' + id + "/"
    return this.http.get(url_api, { headers: this.headers });
  }

  addDocument(doc: Document) {
    const url_api = this.dom.getDomain() + 'api/document/set/insert/'
    return this.http.post(url_api, doc, { headers: this.headers });
  }
  updateDocument(data: Document) {
    const url_api = this.dom.getDomain() + `api/document/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteDocument(id: number) {
    const url_api = this.dom.getDomain() + `api/document/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //  Tipo usuarios

  getUserTypes() {
    const url_api = this.dom.getDomain() + 'api/usertype/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }

  addUserType(data: User) {
    const url_api = this.dom.getDomain() + 'api/usertype/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updateUserType(data: User) {
    const url_api = this.dom.getDomain() + `api/usertype/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteUserType(id: number) {
    const url_api = this.dom.getDomain() + `api/usertype/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //usuarios
  getUsers() {
    const url_api = this.dom.getDomain() + 'api/user/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getUsersByProcess(id) {
    const url_api = this.dom.getDomain() + 'api/user/get/select/process/'+id+'/'
    return this.http.get(url_api, { headers: this.headers });
  }

  addUser(data: User) {
    const url_api = this.dom.getDomain() + 'api/user/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updateUser(data: User) {
    const url_api = this.dom.getDomain() + `api/user/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteUser(id: number) {
    const url_api = this.dom.getDomain() + `api/user/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //Procesos
  getProcesss() {
    const url_api = this.dom.getDomain() + 'api/process/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getProcesssByUser(id) {
    const url_api = this.dom.getDomain() + 'api/process/get/select/user/'+id+'/'
    return this.http.get(url_api, { headers: this.headers });
  }
  addProcess(data: any) {
    const url_api = this.dom.getDomain() + 'api/process/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updateProcess(data: any) {
    const url_api = this.dom.getDomain() + `api/process/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteProcess(id: number) {
    const url_api = this.dom.getDomain() + `api/process/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //fases
  getPhases() {
    const url_api = this.dom.getDomain() + 'api/phase/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getPhasesByProcess(id) {
    const url_api = this.dom.getDomain() + 'api/phase/get/select/process/'+id+'/'
    return this.http.get(url_api, { headers: this.headers });
  }

  addPhase(data: any) {
    const url_api = this.dom.getDomain() + 'api/phase/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updatePhase(data: any) {
    const url_api = this.dom.getDomain() + `api/phase/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deletePhase(id: number) {
    const url_api = this.dom.getDomain() + `api/phase/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //Criterios
  getCriterias() {
    const url_api = this.dom.getDomain() + 'api/criteria/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getCriteriasByProcess(id) {
    const url_api = this.dom.getDomain() + 'api/criteria/get/select/process/'+id+'/'
    return this.http.get(url_api, { headers: this.headers });
  }

  addCriteria(data: any) {
    const url_api = this.dom.getDomain() + 'api/criteria/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updateCriteria(data: any) {
    const url_api = this.dom.getDomain() + `api/criteria/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteCriteria(id: number) {
    const url_api = this.dom.getDomain() + `api/criteria/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  //Indicadores
  getIndicators() {
    const url_api = this.dom.getDomain() + 'api/indicator/get/select/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getIndicatorsByCriteria(cri) {
    const url_api = this.dom.getDomain() + 'api/indicator/get/select/criteria/'+cri+'/'
    return this.http.get(url_api, { headers: this.headers });
  }
  getIndicatorsByProcess(pro) {
    const url_api = this.dom.getDomain() + 'api/indicator/get/select/process/'+pro+'/'
    return this.http.get(url_api, { headers: this.headers });
  }

  addIndicator(data: any) {
    const url_api = this.dom.getDomain() + 'api/indicator/set/insert/'
    return this.http.post(url_api, data, { headers: this.headers });
  }
  updateIndicator(data: any) {
    const url_api = this.dom.getDomain() + `api/indicator/set/update/${data.id}/`
    return this.http.put(url_api, data, { headers: this.headers });
  }
  deleteIndicator(id: number) {
    const url_api = this.dom.getDomain() + `api/indicator/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

    //Tareas
    getTasks() {
      const url_api = this.dom.getDomain() + 'api/task/get/select/'
      return this.http.get(url_api, { headers: this.headers });
    }
    getTaskByPhase(id) {
      const url_api = this.dom.getDomain() + 'api/task/get/select/phase/'+id+'/'
      return this.http.get(url_api, { headers: this.headers });
    }
  
    addTask(data: any) {
      const url_api = this.dom.getDomain() + 'api/task/set/insert/'
      return this.http.post(url_api, data, { headers: this.headers });
    }
    updateTask(data: any) {
      const url_api = this.dom.getDomain() + `api/task/set/update/${data.id}/`
      return this.http.put(url_api, data, { headers: this.headers });
    }
    setStatusTask(data: any,id) {
      const url_api = this.dom.getDomain() + `api/task/set/status/${id}/`
      return this.http.put(url_api, data, { headers: this.headers });
    }
    deleteTask(id: number) {
      const url_api = this.dom.getDomain() + `api/task/set/delete/${id}/`
      return this.http.delete(url_api, { headers: this.headers });
    }




  // COMPARTIENDO METODOS
    sendNotification() {
      const url_api = this.dom.getDomain() + 'api/notification/'
      return this.http.get(url_api, { headers: this.headers });
    }
    report(){
      const url_api = this.dom.getDomain() + 'api/report/'
      return this.http.get(url_api, { headers: this.headers });
    }


    //datos compatidos:

    public idprocess: any = null
    public idphases: any= null
}
