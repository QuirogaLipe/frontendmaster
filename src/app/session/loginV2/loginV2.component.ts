
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/services/auth.service';

@Component({
   selector: 'ms-loginV2-session',
   templateUrl:'./loginV2-component.html',
   styleUrls: ['./loginV2-component.scss'],
   encapsulation: ViewEncapsulation.None
})
export class LoginV2Component {

   email    : string = "";
   password : string = "";


   slideConfig = {"slidesToShow": 1, "slidesToScroll": 1,"autoplay": true, "autoplaySpeed": 1000,"dots":false,"arrows":false};

   sessionSlider : any [] = [
      {
         image : "assets/img/unsa1.jpg",
         name  : "",
         designation : "",
         content : ""
      },
      {
         image : "assets/img/unsa2.jpg",
         name  : "",
         designation : "",
         content : ""
      },
      {
         image : "assets/img/unsa3.jpg",
         name  : "",
         designation : "",
         content : ""
      }
   ]

   constructor( public authService: AuthService,
                public translate : TranslateService,
                public router: Router) { }

   // when email and password is correct, user logged in.
   login(value) {
     // this.authService.loginUser(value);
   }

   onLogin(form){
      console.log(form.value)
      if (form.valid) {
         return this.authService
          .loginuser(this.email, this.password)
          .subscribe(
          data => {
             console.log("login",data)
            //console.log("login me retorna",data)
            //location.assign('/')
            this.authService.setLSuser(data)
            this.authService.setUser(data.username)
            this.router.navigate(['/']);
          },
          error => alert("datos erroneos")
          ); 
      }
   }
}



