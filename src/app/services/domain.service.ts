import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  //private domain: string ="http://18.231.114.82:8000/"
  //private domain: string ="http://34.216.148.9:443/"
  private domain: string ="http://localhost:8000/"
 
  
   constructor() { }
 
   getDomain():string{
 
     return this.domain
   }
 
 }
 